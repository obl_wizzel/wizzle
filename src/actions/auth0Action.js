/**
 * Created by sahidhossen on 8/30/17.
 */
import Auth0 from 'react-native-auth0';
const auth0 = new Auth0({ domain: 'sahid.auth0.com', clientId: 'QBT0nvUHQyWU873PiQdIICfdFtYq3hZ3' });

export function fetchAuth() {
    return function (dispatch){
        auth0
            .webAuth
            .authorize({scope: 'sahidhossenbd@gmail.com', audience: 'https://sahid.auth0.com/userinfo'})
            .then((response ) => {
                dispatch({ type:'FETCH_AUTH_FULFILLED', payload: response.accessToken })
            })
            .catch((error) => {
                dispatch({ type:'FETCH_AUTH_REJECTED', payload: error })
            });
    }
}
