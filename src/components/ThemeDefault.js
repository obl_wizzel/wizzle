/**
 * Created by sahidhossen on 8/23/17.
 */
import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
let windowSize = Dimensions.get('window');


export class ThemeDefaultBackground extends React.Component {
    render() {
        return (
                <Image
                    style={styles.imageBackground}
                    maintainAspectRatio={true}
                    source={require('../../assets/img/rectangle.png')}>
                </Image>
        );
    }
}

export  class ThemeHeaderBackground extends React.Component {
    render() {
        return (
            <Image
                style={styles.imageBackgroundHeader}
                maintainAspectRatio={true}
                source={require('../../assets/img/rectangle_head.png')}>
            </Image>
        );
    }
}


const styles = StyleSheet.create({
    imageBackground:{
        flex: 1,
        position: 'absolute',
        resizeMode: 'cover',
        width: windowSize.width,
        height: windowSize.height,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBackgroundHeader:{
        flex: 1,
        width:windowSize.width,
        resizeMode: 'cover',
        position: 'absolute',
        height:80,
        top:0,

    }
})