import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import FlashScreen from './src/home/FlashScreen';

export default class App extends React.Component {
  render() {
    return (
        <Router>
          <Scene key="root">
            <Scene key="flash_screen" hideNavBar={true} initial component={FlashScreen} title="Flash Screen"/>
          </Scene>
        </Router>
    );
  }
}