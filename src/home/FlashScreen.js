/**
 * Created by sahidhossen on 8/17/17.
 */
import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import {Actions} from 'react-native-router-flux';
import { ThemeDefaultBackground } from '../components/ThemeDefault';
import ThemeStatusBar from '../components/ThemeStatusBar';
import {fetchWizzleSettings} from '../../src/actions/wizzleSettingsAction'

import styles from './style'

@connect( (store) => {
    return {
        wizzleSettings : store.wizzleSettings
    }
})
export default class FlashScreen extends React.Component {
    constructor(props){
        super(props)
    }
    componentDidMount(){
        if( this.props.wizzleSettings.fetched === false )
            this.props.dispatch(fetchWizzleSettings());
    }
    componentDidUpdate(){
        if( this.props.wizzleSettings.wizzleSettings.tokens.length > 0 )
            Actions.buy_screen();
    }
    render() {
        return (
            <View style={styles.container}>
               <ThemeDefaultBackground/>
                <ThemeStatusBar/>
                <View style={ styles.flashScreenRow }>
                    <Image source={require('../../assets/img/logo-mdpi.png')} style={ styles.logo }></Image>
                </View>
                <View style={ styles.flashScreenRow }>
                    <Text style={ styles.version }> Version 1.0 </Text>
                </View>
            </View>
        );
    }
}

