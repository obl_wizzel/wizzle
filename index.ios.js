/**
 * Created by sahidhossen on 8/19/17.
 * Application Name: Wizzle
 * Developer: sahid hossen
 * Developer Email: sahidhossenbd@gmail.com
 * Developer website: sahid.me
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import {Scene, Router} from 'react-native-router-flux';
import { Provider } from 'react-redux';
import FlashScreen from './src/home/FlashScreen';
import SuccessLogin from './src/home/SuccessLogin';
import BuyScreen from './src/buy/BuyScreen';

import store from './store'

export default class Wizzle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Scene key="root">
                        <Scene key="flash_screen" type="transitionToTop" hideNavBar={true} initial component={FlashScreen} title="Flash Screen"/>
                        <Scene key="buy_screen" type="replace" hideNavBar={true}  component={BuyScreen} title="Buy"/>
                        <Scene key="Success_login" hideNavBar={true}  component={SuccessLogin} title="Login Success Full"/>
                    </Scene>
                </Router>
            </Provider>
        );
    }
}

AppRegistry.registerComponent('Wizzle', () => Wizzle);
