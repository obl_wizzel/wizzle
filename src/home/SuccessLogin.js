/**
 * Created by sahidhossen on 8/20/17.
 */
import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar } from 'react-native';
import styles from './style';

export default class SuccessLogin extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.imageBackground}
                    maintainAspectRatio={true}
                    source={require('../../assets/img/rectangle.png')}
                >
                </Image>
                <View style={styles.statusBar}>
                    <StatusBar backgroundColor="transparent" barStyle="light-content" />
                </View>
                <View style={ styles.container }>
                    <Text style={ styles.ofWhiteColor }> Successfully Login </Text>
                </View>
            </View>
        );
    }
}

