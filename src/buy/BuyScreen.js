/**
 * Created by sahidhossen on 8/23/17.
 */
import React from 'react';
import { Text, View, Image, Dimensions, ScrollView, TouchableHighlight, TextInput, Animated, Picker } from 'react-native';
import Auth0 from 'react-native-auth0';
import { connect } from 'react-redux';

import {ThemeHeaderBackground} from '../components/ThemeDefault';
import ThemeStatusBar from '../components/ThemeStatusBar';
import {Tab} from '../components/bottomTabs';

import BuyScreen_1 from '../../src/buy/BuyScreen_1';
import BuyScreen_2 from '../../src/buy/BuyScreen_2';
import BuyScreen_3 from '../../src/buy/BuyScreen_3';

import {fetchAuth} from '../../src/actions/auth0Action'

import styles from './style';


@connect( (store) => {
    return {
        access_token : store.auth0.access_token,
    }
})
export default class BuyScreen extends React.Component {
    constructor( props ){
        super(props);
        this.state = {
            amount:'100',
            selectedTab: 'one',
            accessToken:'',
        }
    }

    onSelectTab(selectedTab) {
        this.setState({ selectedTab })
    }

    onLoadAuth(){
        if(this.props.access_token != null ){
            return false;
        }else {
            this.props.dispatch( fetchAuth() );
        }

    }


    render() {
        console.log( "access token: ", this.props.access_token );
        let tabView = '';
        if( this.state.selectedTab == 'one' )
            tabView = <BuyScreen_1 access_token={this.state.accessToken}  IsLoadAuth0={this.onLoadAuth.bind(this)} />;
        if( this.state.selectedTab == 'two' )
            tabView = <BuyScreen_2/>;
        if( this.state.selectedTab == 'three' )
            tabView = <BuyScreen_3/>;


        return (
            <View style={styles.container}>
                <ThemeHeaderBackground/>
                <ThemeStatusBar/>
                <View style={ styles.header }>
                    <View style={[ styles.column_4,{justifyContent: 'center', alignItems:'flex-start'} ]}>
                        <TouchableHighlight
                            underlayColor={'transparent'}
                            onPress={() => this.appointmentList()}>
                            <Image
                                style={styles.logo}
                                 source={require('../../assets/img/logo.png')}>
                            </Image>
                        </TouchableHighlight>
                    </View>
                    <View style={ [ styles.column, styles.column_1, { alignItems:'flex-end'} ] }>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => this.appointmentList()}>
                            <Image
                                style={styles.avatar}
                                source={require('../../assets/img/avatar.png')}>
                            </Image>
                        </TouchableHighlight>
                    </View>
                    <View style={ [ styles.column, styles.column_1, { alignItems:'flex-end', paddingRight:15} ] }>
                        <TouchableHighlight underlayColor={'transparent'} onPress={() => this.appointmentList()}>
                            <Image
                                style={styles.bar}
                                source={require('../../assets/img/bar.png')}>
                            </Image>
                        </TouchableHighlight>
                    </View>
                </View>

                { tabView }

                <View style={styles.bottomTabBar}>
                    <Tab
                        onTabPress={this.onSelectTab.bind(this)}
                        title="BUY"
                        id="one"
                        isSelected={this.state.selectedTab == "one"}
                        imgSrc={require("../../assets/img/tab_icon_3.png")}
                     />
                    <Tab
                        onTabPress={this.onSelectTab.bind(this)}
                        title="SELL"
                        id="two"
                        isSelected={this.state.selectedTab == "two"}
                        imgSrc={require("../../assets/img/tab_icon_3.png")}
                     />
                    <Tab
                        onTabPress={this.onSelectTab.bind(this)}
                        title="PAY"
                        id="three"
                        isSelected={this.state.selectedTab == "three"}
                        imgSrc={require("../../assets/img/tab_icon_3.png")}
                        lastItem={ true }
                     />
                </View>

            </View>
        );
    }
}

