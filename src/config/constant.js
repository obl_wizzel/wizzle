/**
 * Created by sahidhossen on 9/5/17.
 */

const API_VERSION = "v2";
export const DEFAULT_COUNTRY = "NL";
export const WIZZLE_API_BASE = "https://wizzle.io/api/"+API_VERSION;
