/**
 * Created by sahidhossen on 8/24/17.
 */
import React from 'react';
import { StyleSheet, Image, Dimensions, View, Text, TouchableHighlight  } from 'react-native';
let windowSize = Dimensions.get('window');


export class Tab extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        let style = this.props.isSelected && styles.selectedTab || styles.normalTab;
        let fontStyle = this.props.isSelected && styles.selectedTabText || styles.normalTabText;
        let removeLastItemBorder = ( this.props.id == 'three' ) ? styles.shouldNotBorder : styles.shouldBorder
         return (
            <TouchableHighlight style={[styles.container]} onPress={() => this.props.onTabPress(this.props.id)}>
                <View style={[style,styles.tabStyle, removeLastItemBorder]}>
                    <Image style={styles.icon} source={this.props.imgSrc}/>
                    <Text style={ [styles.tabFontStyle, fontStyle] }>{this.props.title}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:'stretch',
        justifyContent:'center',
    },
    tabStyle: {
        alignItems:'center',
        justifyContent:'center',
        height:46,
        borderRightWidth:1,
        borderColor:'transparent',
        paddingTop:5,
    },
    tabFontStyle: {
        fontSize:13,
        fontWeight:'300'
    },
    selectedTab:{
        backgroundColor:'#EBECEC'
    },
    normalTab: {
        backgroundColor:'#EBECEC'
    },
    icon:{
        height:25,
        width:25,
        marginBottom:5,
    },
    selectedTabText: {
        color:'#000000'
    },
    normalTabText: {
        color:'#acacac'
    },
    shouldBorder: {
        borderColor:'#d7d7d7',
    },
    shouldNotBorder: {
        borderColor:'transparent'
    }
})