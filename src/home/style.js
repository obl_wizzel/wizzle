/**
 * Created by sahidhossen on 8/17/17.
 */
import { StyleSheet, Dimensions, StatusBar, Platform } from 'react-native';
let windowSize = Dimensions.get('window');
const styles =  StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    flashScreenRow: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        position:'relative',
        width: windowSize.width
    },
    logo :{
        width: windowSize.width-50,
        resizeMode:"contain",
        height:100
    },
    version: {
        paddingBottom:25,
        backgroundColor:'transparent',
        color:'rgba(255,255,255,0.8)',
        fontSize:20,
        fontWeight:'300'
    },

    ofWhiteColor:{
        color:'rgba(255,255,255,0.8)',
        backgroundColor:'transparent'
    }
});

module.exports = styles;