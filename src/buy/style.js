/**
 * Created by sahidhossen on 8/23/17.
 */
import { StyleSheet, Dimensions, StatusBar, Platform } from 'react-native';

let windowSize = Dimensions.get('window');

const styles =  StyleSheet.create({
    backgroundTransparent: {
      backgroundColor:'transparent'
    },
    container: {
        flex: 1,
        flexDirection:'column',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        backgroundColor:'#ffffff',
        paddingBottom:56,
    },

    header: {
        height:60,
        flexDirection:'row',
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    column:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    column_1:{
        flex:1,
    },
    column_2:{
        flex:2,
    },
    column_3:{
        flex:3,
    },
    column_4:{
        flex:4,
    },

    logo:{
        width:80,
        resizeMode:'contain',
        marginLeft:10
    },
    avatar:{
        width:20,
        resizeMode:'contain',
        paddingLeft:10,
    },
    bar:{
        width:20,
        resizeMode:'contain',
    },

    buyContainer: {
        flex: 1
    },
    buyContent: {
        flex:1,
        backgroundColor:"#ffffff",
        width:windowSize.width - 20,
        marginLeft:10,
    },
    padding_10: {
        paddingBottom:10,
        paddingTop:10,
    },
    selectTouchableBox: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        height:50,
        width:windowSize.width-20,
        paddingTop:0,
        paddingLeft:10,
        borderWidth:1,
        borderColor:'#9f9f9f',
        borderRadius:20/3,
        overflow:'hidden'
    },
    tokenType: {
        fontSize:20,
        fontWeight:'800',
        paddingLeft:5,
        fontStyle:'italic'
    },
    textInputField: {
        height:50,
        width:windowSize.width-20,
        paddingTop:0,
        paddingLeft:10,
        borderWidth:1,
        borderColor:'#9f9f9f',
        borderRadius:20/3,
        fontSize:20,
        fontWeight:'600'
    },
    bitcoin:{
        width:20,
        resizeMode:'contain',
    },
    mute: {
        color:'#cfcfcf',
        fontSize:16,
    },

    totalCostSection: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        height:50,
        width:windowSize.width-20,
        paddingTop:0,
        borderWidth:1,
        borderColor:'#9f9f9f',
        borderRadius:20/3,
        overflow:'hidden'
    },
    currencyType:{
        width:100,
        backgroundColor:"#EBECEC",
        height:50,
        paddingLeft:10,
        paddingTop:12,
        position:'relative'
    },
    currencyText: {
        fontSize:20,
    },
    currencyValue: {
        flex:4,
        justifyContent:'flex-start',
        alignItems:'stretch',
        position:'relative'
    },
    currencyTextInputField: {
        height:50,
        paddingLeft:10,
        paddingTop:0,
        fontSize:20,
        fontWeight:'800'
    },
    currencyAmount: {
        fontSize:20
    },

    buttonImage: {
        resizeMode: 'cover',
        height:50,
        width:windowSize.width-20,
        justifyContent:'center',
        alignItems:'center'
    },
    colorLight: {
        color:'#ffffff',
    },
    touchableButton: {
        marginTop:20,
        marginBottom:5
    },
    buttonImageHolder: {
        borderColor:'transparent',
        alignItems:'stretch',
        paddingLeft:0,
        overflow:'hidden'
    },

    alertMessageBox:{
        backgroundColor:'transparent',
        borderWidth:1,
        borderColor:'transparent',
        height:50,
        width:windowSize.width-20,
        paddingTop:0,
        paddingLeft:10,
        justifyContent:'center',
        borderRadius:20/3,
        alignItems:'center'
    },
    alertWarning: {
        backgroundColor:'#FFF1F1',
        borderColor:'#fbc9ca',
    },
    alertSuccess: {
        backgroundColor:'#F2FFE8',
        borderColor:'#C0F3AD',
    },
    alertText: {
        fontSize:16,
        fontWeight:'300'
    },
    alertWarningText: {
        color:'#E18384'
    },
    alertSuccessText: {
        color:'#73B94C'
    },
    downArrow: {
        position:'absolute',
        height:15,
        width:15,
        resizeMode: 'cover',
        right:10,
        top:15,
    },

    bottomTabBar: {
        position:'absolute',
        height:56,
        backgroundColor:'#EBECEC',
        borderTopWidth:2,
        borderColor:"#ECECEC",
        bottom:0,
        left:0,
        right:0,
        width:windowSize.width,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        zIndex:0,
    },

    childTimeline: {
        overflow:'hidden',
        height:120,
        position:"absolute",
        width: windowSize.width-20,
        // borderWidth:1,
        // borderColor:'green',
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        zIndex:99,
        bottom:-50,
        backgroundColor:'#ffffff'
    },
    pickerContainerHolder: {
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    pickerColumn: {
        flex:1
    },
    pickerStyle: {
        fontSize:18,
    },
});

module.exports = styles;