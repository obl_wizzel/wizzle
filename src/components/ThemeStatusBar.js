/**
 * Created by sahidhossen on 8/17/17.
 */
import React from 'react';
import { View, StyleSheet, Dimensions, StatusBar, Platform } from 'react-native';

let windowSize = Dimensions.get('window');
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default class ThemeStatusBar extends React.Component {
    render() {
        return (
            <View style={styles.statusBar}>
                <StatusBar backgroundColor="transparent" barStyle="light-content" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
        statusBar: {
            height: STATUSBAR_HEIGHT,
            width:windowSize.width,
            backgroundColor:'rgba(0,0,0,0.1)',
        },
    })


