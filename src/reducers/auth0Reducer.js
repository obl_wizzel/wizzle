/**
 * Created by sahidhossen on 8/30/17.
 */

export default function reducer(state = {
        access_token : null,
        fetching : false,
        fetched: false,
        error: null
    }, action ) {

    switch( action.type ){
        case 'FETCH_AUTH': {
            return { ...state, fetching: true }
        }
        case "FETCH_AUTH_REJECTED": {
            return { ...state, fetching: false, error: action.payload }
        }
        case "FETCH_AUTH_FULFILLED" : {
            return {
                ...state,
                fetching: true,
                fetched: true,
                access_token : action.payload
            }
        }
    }
    return state;

}