/**
 * Created by sahidhossen on 9/5/17.
 */
import axios from 'axios'
import { WIZZLE_API_BASE, DEFAULT_COUNTRY } from '../config/constant'

const API_END_POINT = "/configuration/operator/";

export function fetchWizzleSettings(){
    return function (dispatch) {
        axios.get(WIZZLE_API_BASE+API_END_POINT+DEFAULT_COUNTRY)
            .then( (response) => {
                console.log("response: ", response.data )
                let wizzleSettings = {
                    tokens : response.data.data.operator.tokens,
                    currencies: response.data.data.operator.fiatcurrencies,
                    payment_methods : response.data.data.operator.transactionCosts
                }
                dispatch({ type:'FETCH_AUTH_FULFILLED', payload:wizzleSettings });
            })
            .catch( (error) => {
                dispatch({ type:'FETCH_AUTH_REJECTED', payload: error })
            })
    }
}