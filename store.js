/**
 * Created by sahidhossen on 8/30/17.
 */
import { applyMiddleware, createStore } from 'redux'

import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise'

import reducer from './src/reducers'

const middleware  = applyMiddleware(promise, thunk, logger);

export default createStore(reducer, middleware );
