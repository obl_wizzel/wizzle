/**
 * Created by sahidhossen on 8/24/17.
 */
import React from 'react';
import { Text, View, Image , ScrollView,Dimensions, TouchableHighlight, TextInput, Animated, Picker, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import styles from './style';

let windowSize = Dimensions.get('window');

@connect( (store) => {
    return {
        access_token : store.auth0.access_token,
        wizzleSettings : store.wizzleSettings.wizzleSettings
    }
})
export default class BuyScreen_1 extends React.Component {
    constructor( props ){
        super(props);
        this.state = {
            amount:'100',
            cost:'100',

            coinChoice:'',
            currency:"",
            paymentMethod:"",

            coinChoiceTrigger: false,
            currencyTrigger: false,
            paymentMethodTrigger:false,

            PickerCoinSlideValue: new Animated.Value(-120),
            PickerCurrencySlideValue: new Animated.Value(-120),
            PickerPaymentSlideValue: new Animated.Value(-120),

            notificationTrigger:false,

        }
    }


    pickerBounceAnimate( index ) {
        let toValue=0;

        if( index ==1 ) {

            if( this.state.currencyTrigger ){
                this.setState({ currencyTrigger:false});
                this.renderSlideAnimate(this.state.PickerCurrencySlideValue, -120 );
            }
            if( this.state.paymentMethodTrigger ){
                this.setState({ paymentMethodTrigger:false});
                this.renderSlideAnimate(this.state.PickerPaymentSlideValue, -120 );
            }

            toValue = (this.state.coinChoiceTrigger) ? -120 : 0;
            this.setState({coinChoiceTrigger: !this.state.coinChoiceTrigger});
            this.renderSlideAnimate(this.state.PickerCoinSlideValue, toValue );

        }

        if( index == 2 ){
            if( this.state.coinChoiceTrigger ){
                this.setState({ coinChoiceTrigger:false});
                this.renderSlideAnimate(this.state.PickerCoinSlideValue, -120 );
            }
            if( this.state.paymentMethodTrigger ){
                this.setState({ paymentMethodTrigger:false});
                this.renderSlideAnimate(this.state.PickerPaymentSlideValue, -120 );
            }

            toValue = (this.state.currencyTrigger) ? -120 : 0;
            this.setState({currencyTrigger: !this.state.currencyTrigger});
            this.renderSlideAnimate(this.state.PickerCurrencySlideValue, toValue );

        }

        if( index == 3 ){
            if( this.state.coinChoiceTrigger ){
                this.setState({ coinChoiceTrigger:false});
                this.renderSlideAnimate(this.state.PickerCoinSlideValue, -120 );
            }
            if( this.state.currencyTrigger ){
                this.setState({ currencyTrigger:false});
                this.renderSlideAnimate(this.state.PickerCurrencySlideValue, -120 );
            }

            toValue = (this.state.paymentMethodTrigger) ? -120 : 0;
            this.setState({paymentMethodTrigger: !this.state.paymentMethodTrigger});
            this.renderSlideAnimate(this.state.PickerPaymentSlideValue, toValue );
        }


    }

    renderSlideAnimate(slideState, toValue ){
        Animated.timing(
            slideState,
            {toValue: toValue, duration: 200}
        ).start();
    }

    IsAuthActive(){
        this.setState({notificationTrigger:true});
    }

    /**
     * @return {null}
     */
    Notification(){

        let alertBoxStyle = (this.props.access_token.length > 0 ) ? styles.alertSuccess : styles.alertWarning;
        let alertTextStyle = (this.props.access_token.length > 0 ) ? styles.alertSuccessText : styles.alertWarningText;
        let alertText = (this.props.access_token.length > 0 ) ? "Awaiting approval" : "You Must Login/Signup to Buy";

        return (!this.state.notificationTrigger) ? null : (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    style={styles.touchableButton} onPress={() => this.props.IsLoadAuth0()}>
                    <View style={[styles.alertMessageBox, alertBoxStyle]}>
                        <Text style={[styles.alertText, alertTextStyle]}> {alertText} </Text>
                    </View>
                </TouchableHighlight>
            );

    }

    render(){
        let {currencies, tokens, payment_methods } = this.props.wizzleSettings;

        let Tokens = tokens.map( (token, index) => {
            return <Picker.Item key={index} label={token.name} value={token.name} />
        });
        let Currencies = currencies.map( (cur, index) => {
            return <Picker.Item key={index} label={cur} value={cur} />
        });
        let paymentMethods = payment_methods.map( (method, index) => {
            return <Picker.Item key={index} label={method.paymentMethod} value={method.paymentMethod} />
        });

        let notification = (this.state.notificationTrigger) ? this.Notification() : null;
        let extraHeight = (this.state.notificationTrigger) ? {height:1} : {height:50};

        this.state.coinChoice = (this.state.coinChoice.length > 0) ? this.state.coinChoice : tokens[0].name;
        this.state.currency = (this.state.currency.length > 0) ? this.state.currency : currencies[0];
        this.state.paymentMethod = (this.state.paymentMethod.length > 0) ? this.state.paymentMethod : payment_methods[0].paymentMethod;

        return (

            <View style={ styles.buyContent }>
                <ScrollView style={ styles.buyContainer }
                    ref={(c) => this._scrollView = c}
                    onScroll={this.handleScroll}
                    scrollEventThrottle={16}
                    onContentSizeChange={(width,height) => {
                                   console.log("content height: "+ height + " window height: " + windowSize.height);
                                   let ScrollViewHeight = windowSize.height;
                                   this._scrollView.scrollTo({ y: 0 });

                                   if(height > ScrollViewHeight ){
                                        {/*let needToScroll = height - ScrollViewHeight;*/}
                                        let needToScroll = 60;
                                         console.log("In", needToScroll);
                                        this._scrollView.scrollTo({ y: needToScroll })
                                    }
                } }
                >
                    <KeyboardAvoidingView resetScrollToCoords={{ x: 0, y: 0 }} behavior= {"padding"} style={{flex:1}} contentContainerStyle={{flex: 1}}>
                    <Text style={[ styles.mute, styles.padding_10 ]}> Select token to buy </Text>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        onPress={() => this.pickerBounceAnimate(1)}
                        >
                        <View style={ [styles.selectTouchableBox]}>
                            <Image style={ styles.bitcoin } source={require('../../assets/img/bitcoin_orange.png')}>
                            </Image>
                            <Text style={ styles.tokenType}> {this.state.coinChoice} </Text>
                            <Image style={ styles.downArrow } source={require('../../assets/img/down-arrow.png')}></Image>
                        </View>
                    </TouchableHighlight>
                    <Text style={[ styles.mute, styles.padding_10 ]}> Type an amount </Text>
                    <TextInput
                        style={styles.textInputField}
                        defaultValue={ this.state.amount }
                        onChangeText={(text) => this.setState({amount : text })}
                        value={this.state.amount}
                    />

                    <Text style={[ styles.mute, styles.padding_10 ]}> Total Cost </Text>

                    <View style={styles.totalCostSection }>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        onPress={() => this.pickerBounceAnimate(2)}>
                        <View style={styles.currencyType}>
                            <Text style={ styles.currencyText}> {this.state.currency} </Text>
                            <Image style={ styles.downArrow } source={require('../../assets/img/down-arrow.png')}></Image>
                        </View>
                    </TouchableHighlight>

                    <View style={ styles.currencyValue }>
                    <TextInput
                        style={styles.currencyTextInputField}
                        defaultValue={ this.state.cost }
                        onChangeText={(text) => this.setState({cost : text })}
                        value={this.state.cost}
                    />
                    </View>
                    </View>
                    <Text style={[ styles.mute, styles.padding_10 ]}> Select Payment Method </Text>
                    <TouchableHighlight
                        underlayColor={'transparent'}
                        onPress={() => this.pickerBounceAnimate(3)}>
                        <View style={ styles.selectTouchableBox}>
                            <Image style={ styles.bitcoin } source={require('../../assets/img/bitcoin_orange.png')}></Image>
                            <Text style={ styles.tokenType}> {this.state.paymentMethod} </Text>
                            <Image style={ styles.downArrow } source={require('../../assets/img/down-arrow.png')}></Image>
                        </View>
                    </TouchableHighlight>

                    <TouchableHighlight
                        underlayColor={'transparent'}
                        style={styles.touchableButton} onPress={() => this.IsAuthActive()}>
                        <View style={ [styles.selectTouchableBox, styles.buttonImageHolder ]}>
                            <Image style={styles.buttonImage} source={require('../../assets/img/rectangle_head.png')}>
                                <Text style={ [styles.tokenType, styles.colorLight]}> BUY </Text>
                            </Image>
                        </View>
                    </TouchableHighlight>

                    { notification }

                        <View style={extraHeight}></View>

                    </KeyboardAvoidingView>
                </ScrollView>
                <Animated.View style={[ styles.childTimeline, {bottom: this.state.PickerCoinSlideValue, width:windowSize.width}  ]}>
                    <View style={styles.pickerContainerHolder}>
                        <View style={ styles.pickerColumn }>
                            <Picker
                                itemStyle={ styles.pickerStyle }
                                selectedValue={this.state.coinChoice}
                                onValueChange={(itemValue, itemIndex)=>this.setState({coinChoice: itemValue})}>
                                { Tokens }
                            </Picker>
                        </View>
                    </View>
                </Animated.View>

                <Animated.View style={[ styles.childTimeline, {bottom: this.state.PickerCurrencySlideValue, width:windowSize.width}  ]}>
                    <View style={styles.pickerContainerHolder}>
                        <View style={ styles.pickerColumn }>
                            <Picker
                                itemStyle={ styles.pickerStyle }
                                selectedValue={this.state.currency}
                                onValueChange={(itemValue, itemIndex)=>this.setState({currency: itemValue})}>
                                { Currencies }
                            </Picker>
                        </View>
                    </View>
                </Animated.View>

                <Animated.View style={[ styles.childTimeline, {bottom: this.state.PickerPaymentSlideValue, width:windowSize.width}  ]}>
                    <View style={styles.pickerContainerHolder}>
                        <View style={ styles.pickerColumn }>
                            <Picker
                                itemStyle={ styles.pickerStyle }
                                selectedValue={this.state.paymentMethod}
                                onValueChange={(itemValue, itemIndex)=>this.setState({paymentMethod: itemValue})}>
                                { paymentMethods }
                            </Picker>
                        </View>
                    </View>
                </Animated.View>
            </View>

        )
    }
}