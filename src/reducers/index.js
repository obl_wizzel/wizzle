/**
 * Created by sahidhossen on 8/30/17.
 */
import { combineReducers } from 'redux'

import auth0 from './auth0Reducer'
import user from './userReducer'
import wizzleSettings from './wizzleSettingsReducer'


export default combineReducers({
    wizzleSettings,
    auth0,
    user
})