/**
 * Created by sahidhossen on 9/5/17.
 */

export default function reducer(state = {
    wizzleSettings: {
        tokens: [],
        currencies: [],
        payment_methods: [],
    },
    fetching : false,
    fetched: false,
    error: null
}, action ) {

    switch( action.type ){
        case 'FETCH_AUTH': {
            return { ...state, fetching: true }
        }
        case "FETCH_AUTH_REJECTED": {
            return { ...state, fetching: false, error: action.payload }
        }
        case "FETCH_AUTH_FULFILLED" : {
            return {
                ...state,
                fetching: true,
                fetched: true,
                wizzleSettings : action.payload
            }
        }
    }
    return state;

}